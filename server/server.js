const path = require('path')
const express = require('express')
const app = express()
const publicPath = path.join(__dirname, '..', 'public')
const port = process.env.PORT || 3000

app.use(express.static(publicPath))

//handle the routes ->send every response to index.html 'because routes are handled by react-router
app.get('*', (req, res) => {
  res.sendFile(path.join(publicPath, 'index.html'))
})

// yarn run build:prod, node server/server.js
app.listen(port, () => {
  console.log('Server is up!');
})