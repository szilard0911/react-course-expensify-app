import {createStore} from 'redux'

// Action generators - functions that retur action objects

// const add = ({a, b}, c) => {
//     return a + b + c
// }
// console.log(addEventListener({a: 1, b: 12}, 120));

const incrementCount = ( {incrementBy = 1} = {} ) => ({
    type: 'INCREMENT',
    incrementBy
})

const decrementCount = ( {decrementBy = 1} = {} ) => ({
    type: 'DECREMENT',
    decrementBy
})

const setCount = ( {count} ) => ({
    type: 'SET',
    count
})

const resetCount = () => ({
    type: 'RESET',
})

// Reducers
// 1. reducers are pure functions
// 2. never change state or action
// 3. 
const countReducer = (state = { count : 0 }, action) => {
    switch (action.type) {
        case 'INCREMENT':
        const incrementBy = typeof action.incrementBy === 'number' ? action.incrementBy : 1
            return {
                count: state.count + incrementBy
            }
            break
        case 'DECREMENT':
        const decrementBy = typeof action.decrementBy === 'number' ? action.decrementBy : 1
            return {
                count: state.count - decrementBy
            }
            break
        case 'SET':
            return {
                count: action.count
            }
            break
        case 'RESET':
            return {
                count: 0
            }
            break
        default:
            return state
            break
    }
}


const store = createStore(countReducer)

// it gets called every single time the store changed. If we call unsubsribe() it stops
const unsubscribe = store.subscribe(() => {
    console.log(store.getState());
})

// Redux actions (action name is UPPERCASE and we separate the words with underscores _ )
// We are sending Objects to the store: (if we call dispatch() the createStore() runs again. thats going to be the action parameter)

// store.dispatch({
//     type: 'INCREMENT',
//     incrementBy: 5
// })

store.dispatch(incrementCount({ incrementBy: 5 }))

// store.dispatch({
//     type: 'INCREMENT'
// })

store.dispatch(incrementCount())

// !!! Unsubscribe !!!
//unsubscribe()

// store.dispatch({
//     type: 'DECREMENT'
// })

// store.dispatch({
//     type: 'DECREMENT',
//     decrementBy: 10
// })

store.dispatch(decrementCount())
store.dispatch(decrementCount({ decrementBy: 10 }))

// store.dispatch({
//     type: 'SET',
//     count: 101
// })
store.dispatch(setCount({ count: 101 }))

// store.dispatch({
//     type: 'RESET'
// })
store.dispatch(resetCount())

// stringet is elfogad simán hiába volt számmal inicializálva - nem típusos nyelv
store.dispatch({
    type: 'SET',
    count: '101'
})


