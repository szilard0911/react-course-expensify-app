const promise = new Promise((resolve, reject) => {
  setTimeout(function() {
    resolve({
      name: 'Szilárd',
      text: 'this is my resolved data'
    })
    // reject('something went wrong')
  }, 1500)
})
console.log('before')

promise.then((data) => {
  console.log('1', data)

  return new Promise((resolve, reject) => {
    setTimeout(function () {
      resolve('this is my other promise')
    }, 1500)
  })
}).then((str) => {
  console.log('Does it run? ', str);
}).catch((error) => {
  console.log('error', error)
})
//mindkettő ugyanaz de szerintem a felső olvashatóbb és Andrew szerint is
// promise.then((data) => {
//   console.log('1', data)
// }, (error) => {
//   console.log('error', error)
// })


console.log('after')