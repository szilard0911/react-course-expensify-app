// HOC - Higher Order Component - a component (HOC) that renders another component
// Reuse code
// Render hijacking
// Prop manipulitaion
// Abstract state

import React from 'react'
import ReactDOM from 'react-dom'

// normal component
const Info = (props) => (
    <div>
        <h1>Info</h1>
        <p>The info is: {props.info}</p>
    </div>
)

//HOC component
const withadminWarning = (WrappedComponent) => {
    return (props) => (
        <div>
            { props.isAdmin && <p>This is private info. Please don't share!</p>}
            <WrappedComponent {...props} />
        </div>
    )
}
const AdminInfo = withadminWarning(Info)

// another HOC component
const requireAuthentication = (WrappedComponent) => {
    return (props) => (
        <div>
            { !props.isAuthenticated && <p>Please login to view info!</p>}
            { props.isAuthenticated && <WrappedComponent {...props} />}
        </div>
    )
}
const AuthInfo = requireAuthentication(Info)

// ReactDOM.render(<AdminInfo isAdmin={false} info="There are the detail" />, document.getElementById('app'))
ReactDOM.render(<AuthInfo isAuthenticated={true} info="There are the detail" />, document.getElementById('app'))