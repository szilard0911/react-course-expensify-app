//
// Object Desctructuring
//

// const person = {
//     age: 30,
//     location: {
//         city: 'Budapest',
//         temperature: 25
//     }
// }

// const {name: firstName = 'Anonymous', age} = person
// // const name = person.name
// // const age = person.age
// console.log(`${firstName} is ${age}.`)

// const {city, temperature: temp} = person.location;
// if(city && temp){
//     console.log(`it's ${temp} in ${city}`);
// }

const book = {
    title: 'Ego is the Enemy',
    author: 'Ryan Holiday',
    publisher: {
        name: 'Pinguin'
    }
}

const {name : publisherName = "Self-Publisher"} = book.publisher

console.log(publisherName);

//
// Array Desctructuring
//

const address = ['32 Garam utca', 'Budapest', 'Budapest', '1133']

const [, city, state = 'New York'] = address

console.log(`you are in ${city} ${state}`);


const item = ['Coffee', '$2.00', '$2.50', '$2.75']

const [coffee, , mediumPrice] = item

console.log(`A medium ${coffee} costs ${mediumPrice}`);