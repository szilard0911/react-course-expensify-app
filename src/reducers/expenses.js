// Expenses reducer
// a configureStore.js -ben ehhez rendeltük az expenses tehát a storeban mindig az lesz az expenses amit ez ad vissza amikor dispatchelik az egyik action-jét  az actions/expenses.js -ben
const expensesReducerDefaultState = []
export default (state = expensesReducerDefaultState, action) => {
  switch (action.type) {
    case 'ADD_EXPENSE':
      // return state.concat(action.expense)
      return [
        ...state,
        action.expense
      ]
    case 'REMOVE_EXPENSE':
      //nekem ez jobban tetszik és a meghíváskor is egyértelmű mert nem kell a {id} csak simán id
      // return state.filter((expense) => {
      //     return expense.id != action.id
      // })
      return state.filter(({ id }) => id != action.id)
    case 'EDIT_EXPENSE':
      return state.map((expense) => {
        if (expense.id === action.id) {
          return {
            ...expense,
            ...action.updates
          }
        } else {
          return expense
        }
      })
    case 'SET_EXPENSES':
      return action.expenses
    default:
      return state
  }
}

