import React from 'react'
import { connect } from 'react-redux'
import numeral from 'numeral'
import selectExpenses from '../selectors/expenses'
import selectExpensesTotal from '../selectors/expenses-total';

const getMessage = (expensesCount, expensesTotal) => {
  const expensesWord = expensesCount === 1 ? 'expense' : 'expenses'
  const formatedExpenseTotal = numeral(expensesTotal / 100).format('$0,0.00')
  return `Viewing ${expensesCount} ${expensesWord} totaling ${formatedExpenseTotal}.`
}

export const ExpensesSummary = (props) => (
  <div>
    <h1>{getMessage(props.expensesCount, props.expensesTotal)}</h1>
  </div>
)

const mapStateToProps = (state) => {
  return {
    expensesCount: selectExpenses(state.expenses, state.filters).length,
    expensesTotal: selectExpensesTotal(selectExpenses(state.expenses, state.filters))
  }
}

export default connect(mapStateToProps)(ExpensesSummary)

