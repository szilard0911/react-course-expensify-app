import React from 'react'
import { connect } from 'react-redux'
import ExpenseListItem from './ExpenseListItem';
import selectExpenses from '../selectors/expenses'

// export the unconected version for test. Named export 
export const ExpenseList = (props) => (
    <div>
        <h1>Expense List</h1>
        {
            props.expenses.length === 0 ? (
                <p>No expenses</p>
            ) : (
                props.expenses.map((expense) => {
                    return <ExpenseListItem {...expense} key={expense.id}  />
                })
            )
        }
    
    </div>
)

const mapStateToProps = (state) => {
    return {
        expenses: selectExpenses(state.expenses, state.filters),
    }
    // return {
    //     expenses: state.expenses,
    //     filters: state.filters
    // }
}
// ez itt kurva zavaros nekem: tehát a mapStateToProps-ban határozzuk meg a változókat amiket átadunk az ExpenseListnek de így egxben egya default exportunk is
// a lényeg hogy bármi változik a sateben azt azonnal ki is rendereli
export default connect(mapStateToProps)(ExpenseList)

