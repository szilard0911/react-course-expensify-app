import React from 'react'
import {connect} from 'react-redux'
import ExpenseForm from './ExpenseForm'
import {startAddExpense} from '../actions/expenses';

export class AddExpensePage extends React.Component{
    onSubmit = (expense) => {
        //this.props.dispatch(addExpense(expense))
        // a felsőt haszáltuk csak kellett a módosítás a buzi teszteléshez
        this.props.startAddExpense(expense)
        // redirect to home
        this.props.history.push('/')
    }
    render() {
        return (
            <div>
                <h1>Add Expense</h1>
                <ExpenseForm
                    onSubmit={this.onSubmit}
                />
            </div>
        )
    }
}

// ez is a teszteléshez kell
const mapDispatchToProps = (dispatch) => ({
    startAddExpense: (expense) => dispatch(startAddExpense(expense))
})
// ha connectelünk akkor a fv hozzáfér a propshoz:
export default connect(undefined, mapDispatchToProps)(AddExpensePage)