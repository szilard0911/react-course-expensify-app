// Export a stateless functional componenet
// description, amount, createdAt
import React from 'react'
import {Link} from 'react-router-dom'
import moment from 'moment'
import numeral from 'numeral'

// ha spread-el adunk át akkor fontos hogy legyen {} mert nem lesz jó
const ExpenseListItem = ({description, amount, createdAt, id}) => (
  <div>
    <Link to={`/edit/${id}`} >
      <h3>{description}</h3>
    </Link>
    <p>
      {numeral(amount/100).format('$0,0.00')}
      -
      {moment(createdAt).format('MMMM Do, YYYY')}
    </p> 
  </div>
)

export default ExpenseListItem
