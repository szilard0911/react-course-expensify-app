import React from 'react'
import {connect} from 'react-redux'
import ExpenseForm from './ExpenseForm';
import { startEditExpense, startRemoveExpense } from '../actions/expenses'

export class EditExpensePage extends React.Component{
    onSubmit = (expense) => {
        // itt már a formról elküldött expense-el dolgozunk
        this.props.startEditExpense(this.props.expense.id, expense)
        // redirect to home
        this.props.history.push('/')
    }
    onRemove = (e) => {
        //ez a szar array-t vár pedig egy szaros változó elég lenne
        this.props.startRemoveExpense({id: this.props.expense.id})
        this.props.history.push('/')
    }
    render() {
        return (
            <div>
                <ExpenseForm
                    expense = {this.props.expense}
                    onSubmit={this.onSubmit}
                />
                <button 
                    onClick={this.onRemove}>
                    Remove
                </button> 
            </div>
        )
    }
}

// const EditExpensePage = (props) => {
//     return (
//         <div>
//             <ExpenseForm
//                 expense = {props.expense}
//                 onSubmit={(expense) => {
//                     // itt már a formról elküldött expense-el dolgozunk
//                     props.dispatch(editExpense(props.expense.id, expense))
//                     // redirect to home
//                     props.history.push('/')
//                 }}
//             />
//             <button 
//                 onClick={(e) => {
//                     //ez a szar array-t vár pedig egy szaros változó elég lenne
//                     props.dispatch(removeExpense({id: props.expense.id}))
//                     props.history.push('/')
//                 }}>
//                 Remove
//             </button> 
//         </div>
//     )
// }

// itt nem csak a stateből lesz props hanem új elemekkel is lehet bővíteni
const mapStateToProps = (state, props) => {
    return {
        expense: state.expenses.find((expense) => expense.id === props.match.params.id)
    }
}
const mapDispatchToProps = (dispatch, props) => ({
    startEditExpense: (id, expense) => dispatch(startEditExpense(id, expense)),
    startRemoveExpense: (data) => dispatch(startRemoveExpense(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(EditExpensePage)