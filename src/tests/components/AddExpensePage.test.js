import React from 'react'
import {shallow} from 'enzyme'
import {AddExpensePage} from '../../components/AddExpensePage'
import expenses from '../fixtures/expenses'

let startAddExpense, history, wrapper;

// // jest's before each function
// beforeEach((params) => {
//     startAddExpense = jest.fn()
//     history = { push: jest.fn() }
//     wrapper = shallow(<AddExpensePage startAddExpense={startAddExpense} history={history} />)
// })

test('should render AddExpensePage correctly', () => {
    startAddExpense = jest.fn()
    history = { push: jest.fn() }
    wrapper = shallow(<AddExpensePage startAddExpense={startAddExpense} history={history} />)

    expect(wrapper).toMatchSnapshot()
})

test('should handle onSubmit', () => {
    startAddExpense = jest.fn()
    history = { push: jest.fn() }
    wrapper = shallow(<AddExpensePage startAddExpense={startAddExpense} history={history} />)

    wrapper.find('ExpenseForm').prop('onSubmit')(expenses[1])
    expect(history.push).toHaveBeenLastCalledWith('/')
    expect(startAddExpense).toHaveBeenLastCalledWith(expenses[1])
})
