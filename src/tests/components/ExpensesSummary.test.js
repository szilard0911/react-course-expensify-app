import React from 'react'
import { shallow } from 'enzyme';
import { ExpensesSummary } from '../../components/ExpensesSummary'

test('should correctly render expensesSummary with 1 expense', () => {
  const wrapper = shallow(<ExpensesSummary expensesCount={1} expensesTotal={12345} />)
  expect(wrapper).toMatchSnapshot()
})

test('should correctly render expensesSummary with multiple expenses', () => {
  const wrapper = shallow(<ExpensesSummary expensesCount={3} expensesTotal={123456} />)
  expect(wrapper).toMatchSnapshot()
})