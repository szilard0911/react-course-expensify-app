import React from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'

export const PublicRoute = ({
  isAuthentcated,
  component: Component,
  ...rest
}) => (
    <Route {...rest} component={(props) => (
      isAuthentcated ? (
        <Redirect to="/dashboard" />

      ) : (
          <Component {...props} />
        )
    )} />
  )

const mapStateRouteProps = (state) => ({
  isAuthentcated: !!state.auth.uid
})

export default connect(mapStateRouteProps)(PublicRoute)
