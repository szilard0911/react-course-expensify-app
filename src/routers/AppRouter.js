import React from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'
import NotFoundPage from '../components/404'
import EditExpensePage from '../components/EditExpensePage'
import AddExpensePage from '../components/AddExpensePage'
import ExpenseDashboardPage from '../components/ExpenseDashboardPage'
import LoginPage from '../components/LoginPage'
import PrivateRoute from './PrivateRoute'
import PublicRoute from './PublicRoute'

// exact={true} nagyon fontos. ha nincs beállítva akkor minden ami pl / -vel kezdődik annál meghívja azt a komponents is
// (/create esetében előbb rendereli a homeot aztán a create-et)
// de a Switch felülírja mert ha abban vannak a routok akkor sorban megy végig rajtuk és amint találat van az megjelenik és nem megy tovább, ez a 404 miatt kell mert ott nincs path megadva és az exact se segíthet mivel az a path függvénye

export const history = createHistory()

const AppRouter = () => (
  <Router history={history}>
    <div>
      <Switch>
        <PublicRoute path="/" component={LoginPage} exact={true} />
        <PrivateRoute path="/dashboard" component={ExpenseDashboardPage} />
        <PrivateRoute path="/create" component={AddExpensePage} />
        <PrivateRoute path="/edit/:id" component={EditExpensePage} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </Router>
)

export default AppRouter
  