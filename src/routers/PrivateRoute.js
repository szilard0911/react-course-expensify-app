import React from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'
import Header from '../components/Header'

export const PrivateRoute = ({
  isAuthentcated,
  component: Component,
  ...rest
}) => (
    <Route {...rest} component={(props) => (
      isAuthentcated ? (
        <div>
          <Header />
          <Component {...props} />
        </div>
      ) : (
          <Redirect to="/" />
        )
    )} />
  )

const mapStateRouteProps = (state) => ({
  isAuthentcated: !!state.auth.uid
})

export default connect(mapStateRouteProps)(PrivateRoute)