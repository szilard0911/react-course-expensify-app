// import all named exports under the firebase variable
import * as firebase from 'firebase'

const config = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGE_SENDER_ID
}

firebase.initializeApp(config)

const database = firebase.database()
const googleAuthProvider = new firebase.auth.GoogleAuthProvider()

export { firebase, googleAuthProvider, database as default }


///////////////////////////////////////////////////////////////////////
// //subscribe to child_removed event
// database.ref('expenses').on('child_removed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val())
// })

// //subscribe to child_changed event
// database.ref('expenses').on('child_changed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val())
// })

// //subscribe to child_added event
// database.ref('expenses').on('child_added', (snapshot) => {
//   console.log(snapshot.key, snapshot.val())
// })

// subscribe to value change event
// const onValueChange = database.ref('expenses').on('value', (snapshot) => {
//   const expenses = []

//   snapshot.forEach((childSnapshot) => {
//     expenses.push({
//       id: childSnapshot.key,
//       ...childSnapshot.val()
//     })
//   })
//   console.log(expenses)
// }, (e) => {
//   console.log('Error with data fetching: ', e);
// })


////////////////////////////////////////////////////////////////////
// database.ref('expenses').push({
//   description: 'Expense #01',
//   note: '',
//   amount: 20000,
//   createdAt: 123456
// })

// arrays/objects in firebase
// push creates random ids
// database.ref('notes').push({
//   title: 'To do',
//   body: 'Go for a run'
// })

// id gonna by set
// const firebaseNotes = {
//   notes: {
//     id12345: {
//       title: 'first note',
//       body: 'this is my note'
//     },
//     id67890: {
//       title: 'another note',
//       body: 'this is my note'
//     }
//   }
// }

// id gonna be a number counting form 0
// const notes = [{
//   id: '21',
//   title: 'first note',
//   body: 'this is my note'
// }, {
//   id: '21dsad',
//   title: 'another note',
//   body: 'this is my note'
//   }]
// database.ref('notes').set(notes)



// on function
// const onValueChange = database.ref().on('value', (snapshot) => {
//   const name = snapshot.val().name
//   const title = snapshot.val().job.title
//   const company = snapshot.val().job.company
//   console.log(`${name} is a ${title} at ${company}`)
// }, (e) => {
//   console.log('Error with data fetching: ', e);
//   })

//   setTimeout(function() {
//     database.ref('job/title').set('CEO')
//   }, 2000);

// get data and rerun if the data changes
// const onValueChange = database.ref().on('value', (snapshot) => {
//   console.log(snapshot.val())
// }, (e) => {
//   console.log('Error with data fetching: ', e);
// })

// setTimeout(function() {
//   database.ref('age').set(29)
// }, 3500);
// // unsubscribe from on() function. if you call it without function name it will unsubscribe from all the subscriptions
// setTimeout(function () {
//   database.ref('age').off(onValueChange)
// }, 7000);

// setTimeout(function () {
//   database.ref('age').set(30)
// }, 10500);


// get (all) data
// database.ref()
//   .once('value')
//   .then((snapshot) => {
//     const val = snapshot.val()
//     console.log(val)
//   }).catch((e) => {
//     console.log('Error fetching data: ', e)
//   })


// a set fv a promise
// ref() gives a reference to a specific part of our database. like columns in sql databases
// database.ref().set({
//   name: 'Szilárd Papp',
//   age: 21,
//   stressLevel: 6,
//   job: {
//     title: 'software developer',
//     company: 'Google'
//   },
//   isSingle: false,
//   location: {
//     city: 'Budapest',
//     country: 'Hungary'
//   }
// }).then(() => {
//   console.log('Data is saved.')
// }).catch((e) => {
//   console.log('This failed', e)
// })


// database.ref().update({
//   stressLevel: 9,
//   'job/company': 'Amazon',
//   'location/city': 'Seattle'
// })

// database.ref().update({
//   job: 'manager',
//   'location/city': 'Győr'
// })

// need to call with object
// null-al lehet törölni
// ha a location-ek updateljük csak a city értékét a country is eltűnik
// database.ref().update({
//   job: 'manager', 
//   location: {
//     city: 'Győr'
//   },
//   isSingle: null
// })


// it removes the data: 
// database.ref('isSingle').set(null)

// database.ref().remove()
//   .then(function () {
//     console.log("Remove succeeded.")
//   })
//   .catch(function (error) {
//     console.log("Remove failed: " + error.message)
//   })
