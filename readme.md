
```JSON
package.json - //delete the comments//
"scripts": {
    "serve": "live-server public/",                 // npm run serve
    "build:dev": "webpack",                         // yarn run build:dev
    "build:prod": "webpack -p --env production",    // yarn run build:prod
    "dev-server": "webpack-dev-server",             // yarn run dev-server
    "test": "jest --config=jest.config.json",       // yarn test -- --watch
    "start": "node server/server.js",               // heroku calls npm start automatically
    "heroku-postbuild": "yarn run build:prod",      // heroku makes a build after every push 
    "build-old": "webpack --watch",
    "build-babel-old": "babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch"
  },
```

